import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './pages/MainPage';
import Nav from './pages/Nav';

function App() {
  return (
    <BrowserRouter>
    <div className='grid-container'>
      <div className='grid-nav'>
        <Nav />
      </div>
      <div>
          <Routes>
            <Route path="/" element={<MainPage />} />
          </Routes>
      </div>
    </div>
    </BrowserRouter>
  );
}

export default App;
